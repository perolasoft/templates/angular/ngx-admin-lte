export const NGRX_ADMINLTE_CONFIG_SECURITY_SERVICE_CONTEXT_ICON = 'icons.context';
export const NGRX_ADMINLTE_CONFIG_SECURITY_SERVICE_CONTEXT_LIST_PATH = 'security.service.contextListPath';
export const NGRX_ADMINLTE_CONFIG_SECURITY_SERVICE_CONTEXT_RESOLVE_PATH = 'security.service.contextResolvePath';
