import { NgxAdminLteModuleConfig } from './../../../models/config';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Store, select, createFeatureSelector } from '@ngrx/store';
import { NGX_ADMINLTE_NGRX_SELECTOR_MENU } from '../../../models/utils/constants';
import { Label } from '../../../models/config';
import { ConfigService } from '../../../services/config.service';
import {
  Principal,
  selectSecurityState,
  selectMenuState,
  ItemMenu,
  NgrxTemplateModuleDetailConfig,
  ConfigService as NgrxTemplateConfigService,
} from '@perolasoft/ngrx-template';

declare var $: any;

@Component({
  selector: 'adminlte-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent implements OnInit {
  public config: NgxAdminLteModuleConfig;
  public detail: NgrxTemplateModuleDetailConfig;
  public principal$: Observable<Principal>;
  public menu$: Observable<ItemMenu[]>;
  public labels: Label[];

  constructor(private configService: ConfigService, private store: Store) {
    this.menu$ = store.select(selectMenuState);
    this.principal$ = store.select(selectSecurityState).pipe(
      map(
        (principal) => ({
          ...principal,
          imageUrl: principal && principal.sex
            ? `assets/images/avatar_profile_${principal.sex.toLowerCase()}.svg`
            : 'assets/images/avatar_profile.svg',
        })
      )
    );
    this.config = configService.getConfig();
    this.detail = configService.getConfig().detail;
    //selectSecurityState(store);
  }

  ngOnInit() {
    const sidebarConfig = this.configService.getSideBarConfig();
    this.labels = sidebarConfig.labels ? sidebarConfig.labels : [];

    // Inicializando o Treeview e PushMenu do admin-lte
    // $('[data-widget="treeview"]').Treeview('init');
    // $('[data-widget="pushmenu"]').PushMenu();

  }
}
