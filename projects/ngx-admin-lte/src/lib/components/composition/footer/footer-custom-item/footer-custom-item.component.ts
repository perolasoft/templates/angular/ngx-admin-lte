import { Component, OnInit, ViewContainerRef, ViewChild, Type, Input, ComponentFactory, ComponentFactoryResolver } from '@angular/core';

@Component({
  selector: 'adminlte-footer-custom-item',
  templateUrl: './footer-custom-item.component.html',
  styleUrls: ['./footer-custom-item.component.scss']
})
export class FooterCustomItemComponent implements OnInit {

  @ViewChild('footerCustomItem', {read: ViewContainerRef, static: true })
  private footerCustom: ViewContainerRef;

  @Input()
  private componentType: Type<any>;

  constructor(private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
      const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(this.componentType);
      this.footerCustom.createComponent(factory);
  }

}
