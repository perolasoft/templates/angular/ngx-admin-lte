import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterCustomItemComponent } from './footer-custom-item.component';

describe('FooterCustomItemComponent', () => {
  let component: FooterCustomItemComponent;
  let fixture: ComponentFixture<FooterCustomItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterCustomItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterCustomItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
