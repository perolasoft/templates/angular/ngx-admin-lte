import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderCustomItemComponent } from './header-custom-item.component';

describe('HeaderCustomItemComponent', () => {
  let component: HeaderCustomItemComponent;
  let fixture: ComponentFixture<HeaderCustomItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderCustomItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderCustomItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
