import { NgxAdminLteModuleConfig } from './../../../models/config';
import { Component, OnInit } from '@angular/core';
import { Validators, UntypedFormBuilder } from '@angular/forms';
import {
  SecurityService,
  ConfigService,
  AlertService,
  ExceptionUtil,
  AlertType,
  Alert,
  NGRX_TEMPLATE_CONFIG_ROUTER_LOGIN,
} from '@perolasoft/ngrx-template';
import { Store } from '@ngrx/store';
import { setTheme } from 'ngx-bootstrap/utils';
import { Router, Route, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'adminlte-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss'],
})
export class RecoverPasswordComponent implements OnInit {
  public loginRouter: string;
  public recoverPasswordForm = this.fb.group({
    code: ['', Validators.required],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
  });

  public config: NgxAdminLteModuleConfig;

  constructor(
    private fb: UntypedFormBuilder,
    private securityService: SecurityService,
    private configService: ConfigService,
    private alertService: AlertService,
    private route: ActivatedRoute
  ) {
    this.recoverPasswordForm.controls.code.setValue(
      this.route.snapshot.queryParamMap.get('code')
    );
    this.config = this.configService.getConfig();
    this.loginRouter = configService.getConfigProperty(
      NGRX_TEMPLATE_CONFIG_ROUTER_LOGIN
    );
  }

  ngOnInit(): void {
    setTheme('bs4');

    const bodyElement = document.querySelector('body');
    const classList = bodyElement.classList;
    while (classList.length > 0) {
      classList.remove(classList.item(0));
    }
    classList.add('hold-transition', 'login-page');
  }

  recoverPassword(): void {
    if (this.recoverPasswordForm.valid) {
      const dadosForm = this.recoverPasswordForm.value;
      if (dadosForm.password !== dadosForm.confirmPassword) {
        this.alertService.add(
          new Alert(
            'security',
            AlertType.SUCCESS,
            'security.revocerPassword.formInvalid',
            'O campo de "Senha" não coincide com o "Confirm Senha".'
          )
        );
      } else {
        this.securityService
          .recoverPassword(this.recoverPasswordForm.value)
          .subscribe(
            () => {
              this.alertService.add(
                new Alert(
                  'security',
                  AlertType.SUCCESS,
                  'security.revocerPassword.success',
                  'Senha alterada! Click no link "Login" para iniciar uma sessão.'
                )
              );
            },
            (error) =>
              this.alertService.add(
                ExceptionUtil.createAlert(
                  ExceptionUtil.convertGenericError(error),
                  'security',
                  AlertType.ERROR
                )
              )
          );
      }
    } else {
      this.alertService.add(
        new Alert(
          'security',
          AlertType.SUCCESS,
          'security.revocerPassword.formInvalid',
          'Formulário não preenchido adequadamente.'
        )
      );
    }
  }
}
