import { NGRX_ADMINLTE_CONFIG_SECURITY_SERVICE_CONTEXT_ICON } from './../../../util/constantes';
import { Observable, Subscription, EMPTY } from 'rxjs';
import { ConfigService } from './../../../services/config.service';
import { setTheme } from 'ngx-bootstrap/utils';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  loginAction,
  AlertService,
  Context,
  SecurityService,
  ExceptionUtil,
  AlertType,
  Alert,
} from '@perolasoft/ngrx-template';
import { NgxAdminLteModuleConfig } from '../../../models/config';
import { tap, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'adminlte-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public context: Context = null;
  public contextList: Observable<Context[]> = EMPTY;
  public contextResolveSubscription: Subscription;
  public usernameValueChangeSubscription: Subscription;
  public classIconContext;

  public loginForm: UntypedFormGroup;

  public config: NgxAdminLteModuleConfig;
  public forgotPasswordRouter: string;

  constructor(
    private fb: UntypedFormBuilder,
    private securityService: SecurityService,
    private store: Store<{}>,
    configService: ConfigService,
    alertService: AlertService
  ) {
    this.classIconContext = configService.getConfigProperty(
      NGRX_ADMINLTE_CONFIG_SECURITY_SERVICE_CONTEXT_ICON
    );
    this.config = configService.getConfig();
    this.forgotPasswordRouter = this.config.router.forgotPassword;

    this.loginForm = this.fb.group({
      context: [''],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    setTheme('bs4');

    const bodyElement = document.querySelector('body');
    const classList = bodyElement.classList;
    while (classList.length > 0) {
      classList.remove(classList.item(0));
    }
    classList.add('hold-transition', 'login-page');

    this.contextResolveSubscription = this.securityService
      .contextResolve()
      .subscribe({
        next: (contextSearch) => {
          this.context = contextSearch;
          this.loginForm.controls.context.setValue(contextSearch.id);
        },
        complete: () => {
          if (!this.context) {
            if (this.usernameValueChangeSubscription) {
              this.usernameValueChangeSubscription.unsubscribe();
            }
            this.usernameValueChangeSubscription = this.loginForm.controls.username.valueChanges
              .pipe(debounceTime(1000))
              .subscribe((value) => {
                if (this.loginForm.controls.username.valid) {
                  this.contextList = this.securityService
                    .contextList(value)
                    .pipe(
                      tap((items) => {
                        if (items.length > 0) {
                          this.loginForm.controls.context.setValue(items[0].id);
                        }
                      })
                    );
                }
              });
          }
        },
      });
  }

  ngOnDestroy() {
    if (this.contextResolveSubscription) {
      this.contextResolveSubscription.unsubscribe();
    }
    if (this.usernameValueChangeSubscription) {
      this.usernameValueChangeSubscription.unsubscribe();
    }
  }

  public login() {
    this.store.dispatch(loginAction(this.loginForm.value));
  }
}
