import { NGX_ADMINLTE_NGRX_SELECTOR_BREADCRUMB } from './../../models/utils/constants';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { MenuRoute, selectMenuState, MenuRoutes } from '@perolasoft/ngrx-template';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'adminlte-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, OnDestroy  {

  public menu$: Observable<MenuRoute[]>;
  private subscriptionRouterEvents: Subscription;

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private router: Router,
    private breadcrumbService: BreadcrumbService
  ) {
    this.menu$ = breadcrumbService.itens$;
    // this.menu$ = store.select(selectMenuState);
  }

  ngOnInit() {
    this.subscriptionRouterEvents = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(url => {
        this.configureBreadcrumb();
      });

    this.configureBreadcrumb();
  }

  private configureBreadcrumb() {
    const breadcrumb: MenuRoutes = [
      this.route.routeConfig as MenuRoute,
      ...(this.route.children.map(item => item.routeConfig) as MenuRoutes)
    ];
    this.breadcrumbService.set(breadcrumb);
  }

  ngOnDestroy(): void {
    this.subscriptionRouterEvents.unsubscribe();
  }
}
