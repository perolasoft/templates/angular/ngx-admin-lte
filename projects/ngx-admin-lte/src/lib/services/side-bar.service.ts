import { Injectable, Inject } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { loadMenuAction, ItemMenu } from '@perolasoft/ngrx-template';

@Injectable({
  providedIn: 'root'
})
export class SideBarService {

  // public readonly itens$: Subject<ItemMenu[]> = new BehaviorSubject([]);

  constructor(
    private store: Store<{ menu: ItemMenu[] }>) {
  }

  public load(menu: ItemMenu[]): void {
    this.store.dispatch(loadMenuAction({ menu }));
  }

}
