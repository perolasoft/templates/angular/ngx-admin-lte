import { ConfigService as ConfigTemplateService, ObjectUtil, INJECT_TOKEN_CONFIG } from '@perolasoft/ngrx-template';
import { Injectable, Inject } from '@angular/core';
import { NgxAdminLteModuleConfig, NgxAdminLteModuleSideBarConfig, defaultConfig } from '../models/config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(
    private configTemplateService: ConfigTemplateService,
   // @Inject(INJECT_TOKEN_CONFIG) private config: NgxAdminLteModuleConfig
    ) { }

  public getConfig(): NgxAdminLteModuleConfig {
    return this.configTemplateService.getConfig() ?? defaultConfig;
  }

  public getConfigProperty<T>(key: string): T {
    const result = this.configTemplateService.getConfigProperty<T>(key);
    return result ? result : ObjectUtil.getProperty(defaultConfig, key);
  }

  public getSideBarConfig(): NgxAdminLteModuleSideBarConfig {
    const config = this.getConfig();
    return config.sideBar ? config.sideBar : {};
  }
}
