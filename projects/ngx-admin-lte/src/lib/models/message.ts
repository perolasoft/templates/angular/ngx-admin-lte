import { Person } from './person';

export interface Message {
  description: string;
  date: Date;
  author: Person;
  routeLink: string;
}

export interface SetupModuleNotification {
  groupByType: boolean;
  groups: { type: string, config: { icon: string, routeLink: string } }[];
  maxPreview: number;
  routeLinkAllNotifications: string;
}
