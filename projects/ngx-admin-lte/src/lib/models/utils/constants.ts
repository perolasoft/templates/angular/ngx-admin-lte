export const INJECT_TOKEN_NOTIFICATION_IDENTIFIER = 'ngx-admin-lte:inject-token:notification';

export const NGX_ADMINLTE_NGRX_SELECTOR_MENU = 'NgxAdminLteMenu';
export const NGX_ADMINLTE_NGRX_SELECTOR_BREADCRUMB = 'NgxAdminLteBreadcrumb';
export const NGX_ADMINLTE_NGRX_SELECTOR_MESSAGE = 'NgxAdminLteMessage';
export const NGX_ADMINLTE_NGRX_SELECTOR_NOTIFICATION = 'NgxAdminLteNotification';

export const NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_LEFT = 'template.main.header.itemLeft';
export const NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_CENTER = 'template.main.header.itemCenter';
export const NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_RIGHT = 'template.main.header.itemRight';

export const NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_FOOTER_ITEM_LEFT = 'template.main.footer.itemLeft';
export const NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_FOOTER_ITEM_RIGHT = 'template.main.footer.itemRight';
