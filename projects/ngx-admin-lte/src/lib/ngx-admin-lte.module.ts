import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { CommonModule } from '@angular/common';
import {
  NgModule,
  ModuleWithProviders,
  CUSTOM_ELEMENTS_SCHEMA,
  SkipSelf,
  Optional,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxAdminLteComponent } from './ngx-admin-lte.component';
import { HeaderComponent } from './components/composition/header/header.component';
import { SideBarComponent } from './components/composition/side-bar/side-bar.component';
import { ControlSideBarComponent } from './components/composition/control-side-bar/control-side-bar.component';
import { FooterComponent } from './components/composition/footer/footer.component';
import { MainComponent } from './components/templates/main/main.component';

import { RouterModule } from '@angular/router';
import {
  // NgxAdminLteBsBasicModule,
  NgrxTemplateSecurityModule,
  NgrxTemplateMenuModule,
  NgrxTemplateModule,
  NgrxTemplateBreadcrumbModule,
  NgrxTemplateMessageModule,
  NgrxTemplateNotificationModule,
  INJECT_TOKEN_CONFIG,
  ObjectUtil,
} from '@perolasoft/ngrx-template';
import { NotificationModule } from './modules/notification/notification.module';
import { MessageModule } from './modules/message/message.module';
import { LoginComponent } from './components/templates/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxAdminLteModuleConfig, defaultConfig } from './models/config';
import { HeaderCustomItemComponent } from './components/composition/header/header-custom-item/header-custom-item.component';
import { RecoverPasswordComponent } from './components/templates/recover-password/recover-password.component';
import { SideBarService } from './services/side-bar.service';
import { BreadcrumbService } from './services/breadcrumb.service';
import { ForgotPasswordComponent } from './components/templates/forgot-password/forgot-password.component';
import { FooterCustomItemComponent } from './components/composition/footer/footer-custom-item/footer-custom-item.component';
import { EffectsModule } from '@ngrx/effects';
@NgModule({
  declarations: [
    NgxAdminLteComponent,
    HeaderComponent,
    ControlSideBarComponent,
    FooterComponent,
    MainComponent,
    SideBarComponent,
    BreadcrumbComponent,
    LoginComponent,
    HeaderCustomItemComponent,
    FooterCustomItemComponent,
    RecoverPasswordComponent,
    ForgotPasswordComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
    ReactiveFormsModule,

    //Modules ngx-admin
    NotificationModule,
    MessageModule,

    NgrxTemplateModule,
    // NgxAdminLteBsBasicModule,

    NgrxTemplateSecurityModule,
    NgrxTemplateMenuModule,
    NgrxTemplateBreadcrumbModule,
    NgrxTemplateMessageModule,
    NgrxTemplateNotificationModule,

    // StoreModule,
    // EffectsModule,
    //StoreRouterConnectingModule
  ],
  exports: [
    NgxAdminLteComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    SideBarComponent,
    BreadcrumbComponent,
    LoginComponent,
    HeaderCustomItemComponent,
    FooterCustomItemComponent,
    RecoverPasswordComponent,

    NgrxTemplateModule,
    // NgxAdminLteBsBasicModule,

    NgrxTemplateSecurityModule,
    NgrxTemplateMenuModule,
    NgrxTemplateBreadcrumbModule,
    NgrxTemplateMessageModule,
    NgrxTemplateNotificationModule,
    ForgotPasswordComponent,

    // ngrxTemplateModule.ngModule
  ],
  // schemas:[
  //   CUSTOM_ELEMENTS_SCHEMA
  // ]
})
export class NgxAdminLteModule {

  constructor(@Optional() @SkipSelf() parentModule?: NgxAdminLteModule) {
    if (parentModule) {
      throw new Error(
        'NgxAdminLteModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(
    config?: NgxAdminLteModuleConfig
  ): ModuleWithProviders<NgxAdminLteModule> {
    console.log('NgxAdminLteModule0', defaultConfig, config);
    ObjectUtil.populate(defaultConfig, config, false);
    console.log('NgxAdminLteModule1', defaultConfig, config);
    const ngrxTemplateModule = NgrxTemplateModule.forRoot(defaultConfig);
    console.log('NgxAdminLteModule2', defaultConfig, config, ngrxTemplateModule);
    return {
      ngModule: NgxAdminLteModule,
      providers: [
        // { provide: INJECT_TOKEN_SIDEBAR_LABEL, useValue: config && config.sideBar ? config.sideBar.labels : []},
        ...ngrxTemplateModule.providers,
        SideBarService,
        BreadcrumbService,
      ],
    };
  }
}
