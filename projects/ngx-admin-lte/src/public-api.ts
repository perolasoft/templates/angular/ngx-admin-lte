/*
 * Public API Surface of ngx-admin-lte
 */

export * from './lib/ngx-admin-lte.service';
export * from './lib/ngx-admin-lte.component';
export * from './lib/ngx-admin-lte.module';

export * from './lib/models/config';

// Components exporteds from modules
export * from './lib/components/composition/header/header.component';
export * from './lib/components/composition/footer/footer.component';
export * from './lib/components/notification/notification.component';
export * from './lib/components/composition/header/header-custom-item/header-custom-item.component';
export * from './lib/components/composition/footer/footer-custom-item/footer-custom-item.component';
export * from './lib/components/templates/recover-password/recover-password.component';

// Extra modules
export * from './lib/modules/notification/notification.module';

// components
export * from './lib/components/templates/main/main.component';
export * from './lib/components/composition/side-bar/side-bar.component';
export * from './lib/components/breadcrumb/breadcrumb.component';
export * from './lib/components/templates/login/login.component';
export * from './lib/components/templates/forgot-password/forgot-password.component';

// serives
export * from './lib/services/breadcrumb.service';
export * from './lib/services/side-bar.service';

export * from './lib/redux/effects/index.effects';

