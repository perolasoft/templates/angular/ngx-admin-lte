import { NotificationComponent } from './../../projects/ngx-admin-lte/src/lib/components/notification/notification.component';
import { MessageComponent } from './../../projects/ngx-admin-lte/src/lib/components/message/message.component';
import { NgxAdminLteModuleConfig } from './../../projects/ngx-admin-lte/src/lib/models/config';
import { NgrxTemplateSecurityModule, BsButtonLogoutComponent, ItemMenu, CustomSerializer } from '@perolasoft/ngrx-template';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxAdminLteModule, NgxAdminLteEffects } from 'ngx-admin-lte';

import { IndexComponent } from './components/pages/index/index.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreRouterConnectingModule, NavigationActionTiming } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './app.effects';

const menu: ItemMenu[] = [
  {
    name: 'Index',
    icon: 'fas fa-tachometer-alt',
    path: '/index',
    badge: 'new',
    children: [
      { name: 'Index1', path: '/index/1', icon: 'far fa-circle', badge: 'new' },
      { name: 'Index2', path: '/index/2', icon: 'far fa-circle', badge: '12' },
      { name: 'Index3', path: '/index/3', icon: 'far fa-circle' }
    ]
  }
];

const config: NgxAdminLteModuleConfig = {
  detail: {
    name: 'AdminTLE SC',
    shortName: 'LTE',
    description: 'Showcase AdminLTE',
    version: '0.0.1',
    company: 'PerolaSoft SA',
    companyShortName: 'PerolaSoft'
  },
  template: {
    main: {
      header: {
        itemRight: [MessageComponent, NotificationComponent, BsButtonLogoutComponent]
      }
    }
  },
  icons: {
    context: 'fa fa-building'
  },
  security: {
    service: {
      loginPath: '/api/login',
      contextResolvePath: '/api/security/host-context',
      contextListPath: '/api/security/user-context'
    }
  },
  sideBar: {
    labels: [
      { icon: 'far fa-circle text-danger', text: 'Importante' },
      { icon: 'far fa-circle text-warning', text: 'Alerta' },
      { icon: 'far fa-circle text-info', text: 'Informativo' }
    ]
  }
};

@NgModule({
  declarations: [AppComponent, IndexComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, { metaReducers, runtimeChecks: {
      strictStateImmutability: false,
      strictStateSerializability: false,
      strictActionImmutability: false,
      strictActionSerializability: false
    } }),
    EffectsModule.forRoot([...NgxAdminLteEffects, AppEffects]),
    StoreRouterConnectingModule.forRoot(
      { navigationActionTiming: NavigationActionTiming.PostActivation,
        serializer: CustomSerializer }),
    StoreDevtoolsModule.instrument({ maxAge: 25 , connectInZone: true}),
    // NgrxTemplateModule.forRoot(config),
    NgxAdminLteModule.forRoot(config),
    NgrxTemplateSecurityModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [IndexComponent]
})
export class AppModule {}
